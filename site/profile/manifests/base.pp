# class: profile::base
# Does baseline things

class profile::base {
  include ntp
  include profile::netdata

  if $facts['os']['family'] == 'RedHat' {
    package { ['bash-completion', 'tree', 'zsh']:
      ensure => installed,
    }
  }

  file { ['/etc/puppetlabs/facter', '/etc/puppetlabs/facter/facts.d']:
    ensure => directory,
  }

  profile::user { 'bob':
    ensure  => present,
    uid     => 1847,
    require => Package['zsh'],
  }

  file_line { 'root_ps1':
    ensure => present,
    path   => '/root/.bash_profile',
    line   => 'export PS1="\[\033[48;5;9m\][\u@\h]\\$\[$(tput sgr0)\]"',
    match  => '^(export PS1|PS1)=.*',
  }
}
