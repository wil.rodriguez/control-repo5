# Class: profile::wordpress
#
#
class profile::wordpress {
  include wordpress
  include mysql::server
  include apache
  include apache::mod::php

  package { 'php-mysqlnd':
    ensure => installed,
    notify => Service['httpd'],
  }

  file { '/etc/puppetlabs/facter/facts.d/application.txt':
    ensure => file,
    source => 'puppet:///modules/profile/wordpress/wordpress-application',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  file { '/root/wordpress2':
    ensure  => directory,
    source  => 'puppet:///modules/profile/wordpress2',
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    recurse => true,
  }

  file { '/root/mytemplate':
    ensure  => file,
    content => epp('profile/mytemplate.epp', {
      'keys_enable'     => true,
      'keys_file'       => '/home/blah',
      'keys_trusted'    => ['key1', 'key2'],
      'keys_requestkey' => 'mykey',
      'keys_controlkey' => 'mycontrolkey'})
  }
}
