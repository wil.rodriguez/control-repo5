# Class: profile::netdata
#
#
class profile::netdata {
  exec { 'install netdata':
    command   => 'curl -Ss https://my-netdata.io/kickstart.sh | bash -s all --dont-wait --dont-start-it',
    path      => '/bin/:/usr/bin/',
    unless    => 'test -d /etc/netdata',
    logoutput => true,
  }

  service { 'netdata':
    ensure  => 'running',
    require => Exec['install netdata'],
  }

  exec { 'iptables_netdata':
    command => 'iptables -t nat -I PREROUTING -p tcp --dport 8081 -j REDIRECT --to-ports 19999',
    unless  => 'iptables -L -vt nat | grep 19999',
    path    => '/bin/:/usr/bin/:/sbin',
  }
}
